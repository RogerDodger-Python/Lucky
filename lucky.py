#!/usr/bin/env python3

#lucky.py - Opens several Google search results.
#To Run Code In Prompt:
#       C:\Users\Roger>C:\Python\python.exe C:\Users\Roger\SkyDrive\Programming\Python\Lucky\lucky.py house tree lawn

#   requests:
#       The requests module lets you easily download files
#       from the Web without having to worry about complicated issues
#       such as network errors, connection problems, and data compression.

#   sys:
#       This module provides access to some variables used or maintained
#       by the interpreter and to functions that interact strongly with
#       the interpreter. It is always available.

#   webbrowser:
#       The webbrowser module's open() function can launch a new
#       browser to a specified URL.

#   bs4:
#       Beautiful Soup is a module for extracting information from an HTML
#       page (and is much better for this purpose than regular expressions).


import requests, sys, webbrowser, bs4

#Display text while downloading the Google Page
print('Googling...') #display text while downloading the Google Page
res = requests.get('http://google.com/search?q=' + ' '.join(sys.argv[1:])) #The Join grabs all arguments after 0
res.raise_for_status

#Retrieve top search result links and add the second argument
soup = bs4.BeautifulSoup(res.text, "html.parser")

#Open a browser tab for each result with selecting class r with .r and tag a
linkElems = soup.select('.r a')

#Open a browser tab for each result.
numOpen = min(5, len(linkElems))    #Checks to see if there are either 5 results or less, min functions checks 
for i in range(numOpen):
    webbrowser.open('http://google.com' + linkElems[i].get('href'))